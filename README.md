# Image Picker for Android [![](https://jitpack.io/v/com.gitlab.massarttech/image-picker.svg)](https://jitpack.io/#com.gitlab.massarttech/image-picker)


**Image picker dialog with option to select from Galllery and Camera**
<br/>
<img src="https://gitlab.com/massarttech/image-picker/raw/master/screens/Screen1.png?inline=false" width="300" height="500"/>


## How To Use

### Setup
Add it to your build.gradle with:
```gradle
allprojects {
    repositories {
        maven { url "https://jitpack.io" }
    }
}
```
and:

```gradle
dependencies {
     implementation 'com.gitlab.massarttech:image-picker:1.0.0'
}
```


### Usage
#### Add provider in `AndroidManifest.xml` in `<application></application> 
```xml
<application ....>
    .....
    <provider
        android:name="androidx.core.content.FileProvider"
        android:authorities="${applicationId}.com.massarttech.imagepicker.provider"
        android:exported="false"
        android:grantUriPermissions="true"
        tools:replace="android:authorities">
        <meta-data
            android:name="android.support.FILE_PROVIDER_PATHS"
            android:resource="@xml/picker_provider_paths"/>
    </provider>
    .....
    
```
#### WHen you want to display `ImagePicker` do
```java
PickImageDialog.build(new PickSetup().setTitle("Select Heart Image"))
                        .setOnPickResult(new IPickResult() {
                            @Override
                            public void onPickResult(PickResult r) {
                                imageView.setImageBitmap(r.getBitmap());
                            }
                        })
                        .show(getSupportFragmentManager());
```

****Orignal credit goes to https://github.com/jrvansuita/PickImage****